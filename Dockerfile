FROM perl:5.28.0-slim

WORKDIR /app
COPY cpanfile ./
RUN cpanm --installdeps --quiet --notest .
COPY ./ ./

EXPOSE 3000

CMD perl app.pl daemon

