#!/usr/bin/env perl
use Mojolicious::Lite;

my $counter=0;

get  '/' => sub { shift->render(text => ++$counter); };
del  '/' => sub { shift->render(text => --$counter); };
post '/' => sub { shift->render(text => $counter=0); };

app->start;
