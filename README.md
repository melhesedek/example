# homework app #

## development setup ##

Install dependencies ([cpanm](https://metacpan.org/pod/App::cpanminus#INSTALLATION) is the recommended way)

    cpanm -L local --installdeps --quiet --notest .

start daemon

    perl -I local/lib/perl5 app.pl daemon

## docker setup ##

Build image

    docker build --tag homework-app .

create and run container

    docker run --rm --publish 3000:3000 --name homework-app homework-app

## usage ##

Counter can be [incremented with a browser](http://localhost:3000). Use curl for advanced features

    curl           localhost:3000 # increment
    curl -X DELETE localhost:3000 # decrement
    curl -X POST   localhost:3000 # reset

